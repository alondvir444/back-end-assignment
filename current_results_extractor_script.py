import json
import time

import pandas as pd

from src.data_processors.lot_delta_zones_calculator import calc_lot_delta_zones
from src.data_processors.lot_gnbn_calculator import calc_lot_gnbn
from src.data_processors.lot_radius_calculator import calc_lot_radius
from src.data_processors.z_neighbors_calculator import calc_lot_z_neighbors

TEST_RESOURCES_DIR = "tests/resources"

def save_calc_lot_delta_zones_output(df):
    zone_delta_to_wafers, zone = calc_lot_delta_zones(x=df["X_COORD"].values, y=df["Y_COORD"].values, wafer=df["WAFER"].values, target_values=df["TEST_PASSED"].values)

    with open(f"{TEST_RESOURCES_DIR}/lot_delta_zones_expected_output.json", "w") as f:
        json.dump({
            "zone": zone,
            "zone_delta_to_wafers": zone_delta_to_wafers
        }, f)


def save_calc_lot_radius_output(df):
    radius = calc_lot_radius(df)

    with open(f"{TEST_RESOURCES_DIR}/radius_expected_output.json", "w") as f:
        json.dump(radius, f)


def save_calc_lot_gnbn_output(df):
    neighbors_mean_yield, n_neighbors, n_good_neighbors = calc_lot_gnbn(x=df["X_COORD"].values, y=df["Y_COORD"].values, wafer=df["WAFER"].values, target_values=df["TEST_PASSED"].values)

    with open(f"{TEST_RESOURCES_DIR}/lot_gnbn_expected_output.json", "w") as f:
        json.dump({
            "neighbors_mean_yield": [float(x) for x in neighbors_mean_yield],
            "n_neighbors": n_neighbors,
            "n_good_neighbors": [int(n) for n in n_good_neighbors]
        }, f)
    
def save_calc_lot_z_neighbors_output(df):
    z_neighbors = calc_lot_z_neighbors(df)

    z_neighbors.to_parquet(f"{TEST_RESOURCES_DIR}/lot_z_neighbors_expected_output.parquet")

    df = pd.read_parquet("manufacturing_data_2_categories.parquet")


def save_split_channels_output(df,channels=8,pattern="_CH_",sn='SN',verbose=True):
    """
    """
    start_time = time.time()

    cols_found = [col for col in df.columns if pattern in col] 
    items = []
    count = 0
    rows = df.shape[0]

    col_found_to_end_reg = {
        col: pattern+col.split(pattern)[-1]
        for col in cols_found
    }

    col_found_to_col_name =  {
        col: col.split(pattern)[0]
        for col in cols_found
    }

    for row in df[[sn]+cols_found].itertuples():
        path = f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_{count}.parquet"
        count+=1
        for i in range(1,1+channels):
            item = {}
            item[sn] = getattr(row, sn)
            item['CHANNEL']=i
            reg = pattern+str(i)
            for col in cols_found:
                if col_found_to_end_reg[col] == reg:
                    col_name = col_found_to_col_name[col]
                    item[col_name] = str(getattr(row, col))
                    
            items.append(item)
        if verbose and count%10000==0:
            print("Iterated "+str(count)+"/"+str(rows) + " records")
            df_channels = pd.DataFrame(items)
            df_channels.to_parquet(path)
            items = []
    df_channels = pd.DataFrame(items)
    df_channels.to_parquet(f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_{count}.parquet")
    
    with open(f"{TEST_RESOURCES_DIR}/split_channels_expected_output__cols_found.json", "w") as f:
        json.dump(cols_found, f)
    

if __name__ == "__main__":
    import os

    os.makedirs(f"{TEST_RESOURCES_DIR}", exist_ok=True)

    manufacturing_data_1 = pd.read_parquet("manufacturing_data_1.parquet")
    save_calc_lot_delta_zones_output(manufacturing_data_1)
    save_calc_lot_radius_output(manufacturing_data_1)
    save_calc_lot_gnbn_output(manufacturing_data_1)
    save_calc_lot_z_neighbors_output(manufacturing_data_1)

    os.makedirs(f"{TEST_RESOURCES_DIR}/explode", exist_ok=True)
    manufacturing_data_2 = pd.read_parquet("manufacturing_data_2.parquet")
    manufacturing_data_2 = manufacturing_data_2.astype({col: 'category' for col in manufacturing_data_2.select_dtypes([object]).columns})
    save_split_channels_output(manufacturing_data_2)

    paths = [
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_10000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_20000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_30000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_40000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_50000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_60000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_70000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_80000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_90000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_100000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_110000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_120000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_130000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_140000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_150000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_160000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_170000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_180000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_190000.parquet",
        f"{TEST_RESOURCES_DIR}/explode/split_channels_expected_output_190059.parquet",
    ]

    dfs = [pd.read_parquet(path) for path in paths]
    output_df = pd.concat(dfs).reset_index(drop=True)
    output_df.to_parquet("C:/Users/alond/Alon/data_processing/back-end-assignment/tests/resources/split_channels_expected_output.parquet")
