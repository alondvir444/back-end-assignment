import math
import numpy as np
import pandas as pd


def _calc_zone_per_unit(row, x_mid, y_mid):
    range_center = math.sqrt((1 - row['X_COORD'] / x_mid) ** 2 + (1 - row['Y_COORD'] / y_mid) ** 2)

    if range_center < (math.sqrt(2) * 0.2):
        return 1
    elif range_center < (math.sqrt(2) * 0.4):
        return 2
    elif range_center < (math.sqrt(2) * 0.6):
        return 3
    return 4


def calc_lot_delta_zones(x, y, wafer, target_values):
    """
    The function identifies whether a unit on the dataset is a part of "low yield" zone against it's wafer.
    The function calculate the average yield of each zone divided by the total wafer yield.

    :param x: Series of values representing the 'X_COORD' scale in a lot.
    :type x: array-like of shape (n_samples, )

    :param y: Series of values representing the 'Y_COORD' scale in a lot.
    :type y: array-like of shape (n_samples, )

    :param actual_values: Series of values representing the 'WAFER' in a lot.
    :type actual_values: array-like of shape (n_samples, )

    :param target_values: Target values (usually "TEST_PASSED")
    :type target_values: array-like of shape (n_targets, )

    :return:
        - GOOD_ZONE_DELTA_TO_WAFER - Delta zone yield to Wafer yield calculation for each sample.
        - ZONE - Zone (1-4) in the wafer the unit belongs to after division.
    :rtype:
        - GOOD_ZONE_DELTA_TO_WAFER - array, shape (n_samples,)
        - ZONE - array, shape (n_samples,)
    """
    SUM_KEY = 'SUM'
    COUNT_KEY = 'COUNT'
    MEAN_KEY = 'MEAN'

    max_x = np.max(x)
    max_y = np.max(y)
    min_x = np.min(x)
    min_y = np.min(y)
    x_mid = (max_x + min_x) * 0.5
    y_mid = (max_y + min_y) * 0.5

    zones_list = []
    delta_to_wafer = []
    pairs_dict = {}
    index_pair = []
    zone_dict = {}
    wafer_dict = {}
    for i in range(len(target_values)):
        pair = (x[i], y[i], wafer[i])
        pairs_dict[pair] = target_values[i]

        row = {
            'X_COORD': x[i],
            'Y_COORD': y[i]
        }

        zone = _calc_zone_per_unit(row, x_mid, y_mid)
        zones_list.append(zone)

        pair_zone = (wafer[i], zone)
        index_pair.append(pair_zone)

        if wafer[i] not in wafer_dict:
            wafer_dict[wafer[i]] = {
                SUM_KEY: 0,
                COUNT_KEY: 0
            }
        wafer_dict[wafer[i]][SUM_KEY] += target_values[i]
        wafer_dict[wafer[i]][COUNT_KEY] += 1

        if zone not in zone_dict:
            zone_dict[zone] = {
                SUM_KEY: 0,
                COUNT_KEY: 0
            }
        zone_dict[zone][SUM_KEY] += target_values[i]
        zone_dict[zone][COUNT_KEY] += 1

    for i in range(len(index_pair)):
        pair = index_pair[i]
        _wafer = pair[0]
        _zone = pair[1]

        ratio = wafer_dict[_wafer][SUM_KEY] / wafer_dict[_wafer][COUNT_KEY]
        wafer_dict[_wafer][MEAN_KEY] = ratio

        ratio = zone_dict[_zone][SUM_KEY] / zone_dict[_zone][COUNT_KEY]
        zone_dict[_zone][MEAN_KEY] = ratio

        yield_zone_to_yield_wafer = zone_dict[_zone][MEAN_KEY] / wafer_dict[_wafer][MEAN_KEY]
        delta_to_wafer.append(yield_zone_to_yield_wafer)

    return delta_to_wafer, zones_list


def calc_lot_delta_zones__optimized__vectorized(x: np.array, y: np.array, wafer: np.array, target_values: np.array) -> tuple[np.array, np.array]:
    """
    The function identifies whether a unit on the dataset is a part of "low yield" zone against it's wafer.
    The function calculate the average yield of each zone divided by the total wafer yield.

    :param x: Series of values representing the 'X_COORD' scale in a lot.
    :type x: array-like of shape (n_samples, )

    :param y: Series of values representing the 'Y_COORD' scale in a lot.
    :type y: array-like of shape (n_samples, )

    :param actual_values: Series of values representing the 'WAFER' in a lot.
    :type actual_values: array-like of shape (n_samples, )

    :param target_values: Target values (usually "TEST_PASSED")
    :type target_values: array-like of shape (n_targets, )

    :return:
        - GOOD_ZONE_DELTA_TO_WAFER - Delta zone yield to Wafer yield calculation for each sample.
        - ZONE - Zone (1-4) in the wafer the unit belongs to after division.
    :rtype:
        - GOOD_ZONE_DELTA_TO_WAFER - array, shape (n_samples,)
        - ZONE - array, shape (n_samples,)
    """

    max_x = np.max(x)
    max_y = np.max(y)
    min_x = np.min(x)
    min_y = np.min(y)
    x_mid = (max_x + min_x) * 0.5
    y_mid = (max_y + min_y) * 0.5

    range_center = np.sqrt((1 - x / x_mid) ** 2 + (1 - y / y_mid) ** 2)

    zone = pd.Series(4, index=range(len(target_values)), dtype=int)
    zone_1_selector = range_center < (math.sqrt(2) * 0.2)
    zone_2_selector = (range_center < (math.sqrt(2) * 0.4)) & ~zone_1_selector
    zone_3_selector = (range_center < (math.sqrt(2) * 0.6)) & ~zone_2_selector & ~zone_1_selector

    zone.loc[zone_3_selector] = 3
    zone.loc[zone_2_selector] = 2
    zone.loc[zone_1_selector] = 1

    zones_list = zone.values


    target_values_series = pd.Series(target_values)
    
    zone_mean = target_values_series.groupby(zone).transform('mean')
    wafer_mean = target_values_series.groupby(wafer).transform('mean')

    yield_zone_to_yield_wafer = zone_mean / wafer_mean
    return yield_zone_to_yield_wafer.values, zones_list
