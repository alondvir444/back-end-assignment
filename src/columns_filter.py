from dataclasses import dataclass
from typing import Optional


@dataclass
class ColumnsFilter:
    sn: Optional[str] = None
    uid: Optional[int] = None
