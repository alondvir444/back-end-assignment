# back-end-assignment

In order to run the docker and integration tests, run
`bash run_integration_test.bash`



Potential issues that should be double checked:
* calc_lot_gnbn(): The SN dimension is being reduced. The value taken for each calculation is the (randomally ordered?) LAST value from every SN, instead of some sum,mean etc.
* Some channel-params have mixed data types. e.g. CHANNEL_PARAM_9 is sometimes float and sometimes string. this may imply some data corruption (maybe different versions of the data on the same place)

Shortcuts made from time saving considerations:
* Added SQL filters as string without parameterization - exposes risk of SQL injection.
* Both API and SQL access should be accessed using asyncio
* Added only 1 field for filtering inputs for the different endpoints - "SN". Further fields and filters should be added.

ASSUMPTIONS:
* in `def calc_lot_radius(df, x_scale="X_COORD", y_scale="Y_COORD"):`
both `x_scale` and `y_scale` are not used.
They are assumed to be redundant.
* The exploded columns are treated as strings, and there is no bug in the input (some input columns are floats and some are objects)
* In manufacturing_data_2 and split_channels:
    * assuming that the output order doesn't matter
* In calc_lot_delta_zones() - assuming `n_samples==len(X0)==n_targets==len(y)` as implicitly assumed in the code



