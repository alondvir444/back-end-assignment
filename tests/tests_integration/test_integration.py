import pandas as pd

from tests.tests_integration import service_test_wrapper

from tests.fixtures.expected_outputs import lot_delta_zones_expected_output
from tests.fixtures.expected_outputs import lot_gnbn_expected_output
from tests.fixtures.expected_outputs import radius_expected_output
from tests.fixtures.expected_outputs import split_channels_expected_output
from tests.fixtures.expected_outputs import z_neighbors_expected_output



def test_integration__sanity__calc_lot_delta_zones(lot_delta_zones_expected_output):
    # Arrange
    expected_response = {
        "is_success": True,
        "content": lot_delta_zones_expected_output,
        "message": ""
    }

    # Act
    actual_data = service_test_wrapper.delta_zones()
    
    # Assert
    assert actual_data == expected_response


def test_integration__sanity__calc_lot_gnbn(lot_gnbn_expected_output):
    # Arrange
    expected_response = {
        "is_success": True,
        "content": lot_gnbn_expected_output,
        "message": ""
    }

    # Act
    actual_data = service_test_wrapper.good_and_bad_neighbors()

    # Assert
    assert actual_data == expected_response


def test_integration__sanity__calc_lot_radius(radius_expected_output):
    # Arrange
    expected_response = {
        "is_success": True,
        "content": radius_expected_output,
        "message": ""
    }


    # Act
    actual_radius = service_test_wrapper.lot_radius()
    
    # Assert
    assert actual_radius == expected_response


def test_integration__sanity__split_channels(split_channels_expected_output):
    # Arrange
    expected_df, expected_cols_found = split_channels_expected_output
    _split_channels_test(expected_df, expected_cols_found, sn="HGYA26YGXJ8ZDQS")


def test_integration__split_channels(split_channels_expected_output):
    # Arrange
    expected_df, expected_cols_found = split_channels_expected_output
    _split_channels_test(expected_df, expected_cols_found)

def _split_channels_test(expected_df, expected_cols_found, sn: str = None):
    # Arrange
    if sn is not None:
        expected_df = expected_df.loc[expected_df["SN"] == sn]

    expected_col_to_values_list = expected_df.to_dict(orient="list")

    expected_content = {
            "cols_found": expected_cols_found,
            "values": expected_col_to_values_list
    }
    expected_response = {
        "is_success": True,
        "content": expected_content,
        "message": ""
    }


    # Act
    actual_res = service_test_wrapper.split_channels(sn)

    # Assert
    for expected_field_name, expected_field_values in expected_response['content']['values'].items():
        actual_values = actual_res['content']['values'][expected_field_name]
        for expected_value, actual_value in zip(expected_field_values, actual_values):
            if type(expected_value) == str and type(actual_value) != str:
                assert expected_value == str(actual_value)
            else:
                assert expected_value == actual_value


def test_integration__sanity__calc_lot_z_neighbors(z_neighbors_expected_output):
    # Arrange
    expected_response = {
        "is_success": True,
        "content": z_neighbors_expected_output.to_dict(orient="list"),
        "message": ""
    }

    # Act
    actual_z_neighbors = service_test_wrapper.z_neighbors()
    
    # Assert
    assert actual_z_neighbors == expected_response

