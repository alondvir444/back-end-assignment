
from src.data_processors.lot_delta_zones_calculator import calc_lot_delta_zones__optimized__vectorized
from tests.fixtures.manufacturing_data import manufacturing_data_1
from tests.fixtures.expected_outputs import lot_delta_zones_expected_output


def test_calc_lot_delta_zones(manufacturing_data_1, lot_delta_zones_expected_output):

    # Act
    zone_delta_to_wafers, zone = calc_lot_delta_zones__optimized__vectorized(
        x=manufacturing_data_1["X_COORD"].values,
        y=manufacturing_data_1["Y_COORD"].values,
        wafer=manufacturing_data_1["WAFER"].values,
        target_values=manufacturing_data_1["TEST_PASSED"].values
    )
    

    # Assert
    actual_data = {
        "zone": zone,
        "zone_delta_to_wafers": zone_delta_to_wafers
    }
    assert all(lot_delta_zones_expected_output['zone'] == actual_data['zone'])
    assert all(lot_delta_zones_expected_output['zone_delta_to_wafers'] == actual_data['zone_delta_to_wafers'])

