import requests
import json


HOST = "127.0.0.1"
PORT = 32768

URL = f"http://{HOST}:{PORT}"



def z_neighbors():
    return _create_get_request(f"{URL}/z_neighbors")


def lot_radius():
    return _create_get_request(f"{URL}/lot_radius")


def delta_zones():
    return _create_get_request(f"{URL}/delta_zones")


def good_and_bad_neighbors():
    return _create_get_request(f"{URL}/good_and_bad_neighbors")


def split_channels(sn: str = None):
    url = f"{URL}/split_channels"
    if sn is not None:
        url = f"{url}?sn={sn}"
    return _create_get_request(url)


def _create_get_request(url: str):
    try:
        print(f"Request URL: {url}")
        resp = requests.get(url)
        res = _load_response(resp)
        print(f"Got response to URL: {url}, with length: {len(res['content'])}")
        return _load_response(resp)
    except Exception as e:
        print(f"Failed to get response from URL: {url}")
        print(e)
        return None


def _load_response(resp: requests.Response):
    return json.loads(resp.content)