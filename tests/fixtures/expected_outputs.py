import json

import pandas as pd
import pytest


@pytest.fixture
def lot_delta_zones_expected_output():
    with open("tests/resources/lot_delta_zones_expected_output.json") as f:
        return json.load(f)


@pytest.fixture
def lot_gnbn_expected_output():
    with open("tests/resources/lot_gnbn_expected_output.json") as f:
        return json.load(f)


@pytest.fixture
def radius_expected_output():
    with open("tests/resources/radius_expected_output.json") as f:
        return json.load(f)


@pytest.fixture
def split_channels_expected_output():

    with open("tests/resources/split_channels_expected_output__cols_found.json") as f:
        cols_found =  json.load(f)

    df_channels = pd.read_parquet("tests/resources/split_channels_expected_output.parquet")

    return df_channels, cols_found


@pytest.fixture
def z_neighbors_expected_output():
    return pd.read_parquet("tests/resources/z_neighbors_expected_output.parquet")
