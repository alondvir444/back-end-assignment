import numpy as np
import pandas as pd

def calc_lot_radius(df, x_scale="X_COORD", y_scale="Y_COORD"):
    """
    Calculates the radius for each sample on the wafer by it's (X,Y) WAFER coordinates in one LOT.

    :param df: Input dataset.
    :type df: pandas.DataFrame

    :param x_scale: Feature name in dataset representing the 'X' scale on a WAFER.
    :type x_scale: str, default 'X'

    :param y_scale: Feature name in dataset representing the 'Y' scale on a WAFER.
    :type y_scale: str, default 'Y'

    :return: Radius calculation for each sample.
    :rtype: array-like of shape (n_samples, )
    """
    max_x = df["X_COORD"].max()
    min_x = df["X_COORD"].min()
    x0 = (max_x - min_x) / 2
    max_y = df["Y_COORD"].max()
    min_y = df["Y_COORD"].min()
    y0 = (max_y - min_y) / 2
    
    radii = []
    for index, row in df.iterrows():
        radius = np.sqrt((row["X_COORD"] - x0)**2 + (row["Y_COORD"] - y0)**2)
        radii.append(radius)
    return radii

def calc_lot_radius_optimized(df: pd.DataFrame) -> np.array:
    x_series = df["X_COORD"]
    x0 = (x_series.max() - x_series.min()) / 2
    y_series = df["Y_COORD"]
    y0 = (y_series.max() - y_series.min()) / 2
    
    dist_x = x_series - x0
    dist_y = y_series - y0

    radii = np.sqrt(dist_x**2 + dist_y**2)
    return radii.values
