import pandas as pd

from src.data_processors.z_neighbors_calculator import calc_lot_z_neighbors_optimized
from tests.fixtures.manufacturing_data import manufacturing_data_1
from tests.fixtures.expected_outputs import z_neighbors_expected_output


def test_calc_lot_z_neighbors(manufacturing_data_1, z_neighbors_expected_output):

    # Act
    actual_df = calc_lot_z_neighbors_optimized(manufacturing_data_1)

    # Assert
    pd.testing.assert_frame_equal(z_neighbors_expected_output, actual_df)

