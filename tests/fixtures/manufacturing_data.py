import pytest
import pandas as pd

@pytest.fixture
def manufacturing_data_1():
    return pd.read_parquet('manufacturing_data_1.parquet')

@pytest.fixture
def manufacturing_data_2():
    return pd.read_parquet('manufacturing_data_2.parquet')