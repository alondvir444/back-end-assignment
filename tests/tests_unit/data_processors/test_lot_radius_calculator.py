
from src.data_processors.lot_radius_calculator import calc_lot_radius_optimized
from tests.fixtures.manufacturing_data import manufacturing_data_1
from tests.fixtures.expected_outputs import radius_expected_output


def test_calc_lot_radius(manufacturing_data_1, radius_expected_output):

    # Act
    actual_radius = calc_lot_radius_optimized(manufacturing_data_1)

    # Assert
    assert all(radius_expected_output == actual_radius)

