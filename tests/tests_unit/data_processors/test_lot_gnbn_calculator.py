
from src.data_processors.lot_gnbn_calculator import calc_lot_gnbn__optimized
from tests.fixtures.manufacturing_data import manufacturing_data_1
from tests.fixtures.expected_outputs import lot_gnbn_expected_output


def test_calc_lot_gnbn(manufacturing_data_1, lot_gnbn_expected_output):

    # Act
    neighbors_mean_yield, n_neighbors, n_good_neighbors = calc_lot_gnbn__optimized(
        x=manufacturing_data_1["X_COORD"].values,
        y=manufacturing_data_1["Y_COORD"].values,
        wafer=manufacturing_data_1["WAFER"].values,
        target_values=manufacturing_data_1["TEST_PASSED"].values
    )
    actual_lot_gnbn = {
            "neighbors_mean_yield": neighbors_mean_yield,
            "n_neighbors": n_neighbors,
            "n_good_neighbors": [int(n) for n in n_good_neighbors]
    }

    # Assert
    assert all(lot_gnbn_expected_output["neighbors_mean_yield"] == actual_lot_gnbn["neighbors_mean_yield"])
    assert all(lot_gnbn_expected_output["n_neighbors"] == actual_lot_gnbn["n_neighbors"])
    assert lot_gnbn_expected_output["n_good_neighbors"] == actual_lot_gnbn["n_good_neighbors"]

