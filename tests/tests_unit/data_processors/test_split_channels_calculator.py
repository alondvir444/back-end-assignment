import pandas as pd
from src.data_processors.split_channels_calculator import split_channels__optimized__iterate_by_channels
from tests.fixtures.manufacturing_data import manufacturing_data_2
from tests.fixtures.expected_outputs import split_channels_expected_output


def test_split_channels(manufacturing_data_2, split_channels_expected_output):
    # Arrange
    expected_df, expected_cols_found = split_channels_expected_output

    # Act
    actual_df, cols_found = split_channels__optimized__iterate_by_channels(manufacturing_data_2)

    # Assert
    assert sorted(cols_found) == sorted(expected_cols_found)

    _compare_dfs(expected_df.astype('string').astype('category'), actual_df.astype('string').astype('category'))

def test_split_channels__sanity(manufacturing_data_2, split_channels_expected_output):
    # Arrange
    expected_df, expected_cols_found = split_channels_expected_output
    expected_cols_found = expected_cols_found

    expected_df = expected_df.loc[expected_df["SN"] == "HGYA26YGXJ8ZDQS"]
    manufacturing_data_2 = manufacturing_data_2.loc[manufacturing_data_2["SN"] == "HGYA26YGXJ8ZDQS"]

    # Act
    actual_df, cols_found = split_channels__optimized__iterate_by_channels(manufacturing_data_2)

    # Assert
    assert sorted(cols_found) == sorted(expected_cols_found)

    _compare_dfs(expected_df.astype('string').astype('category'), actual_df.astype('string').astype('category'))

def _compare_dfs(expected_df: pd.DataFrame, actual_df: pd.DataFrame):
    assert set(expected_df.columns) == set(actual_df.columns)
    assert len(expected_df) == len(actual_df)

    expected_df, actual_df = _set_identical_columns_order(expected_df, actual_df)

    _compare_types(expected_df, actual_df)

    is_both_null_df = expected_df.isnull() & actual_df.isnull()
    is_value_euqal_df = (expected_df == actual_df).fillna(False)
    is_dfs_equal_df = is_value_euqal_df | is_both_null_df
    is_dfs_different_df = ~is_dfs_equal_df
    difference_selector = is_dfs_different_df.any(axis='columns')

    if difference_selector.any():
        with pd.option_context('display.max_rows', None, 'display.max_columns', None):
            print(
                f"Found differences in {difference_selector.sum()}/{len(difference_selector)} values. first diff:\n"
                f"expected:\n"
                f"{expected_df.loc[difference_selector].iloc[0]}\n\n"
                f"actual:\n"
                f"{actual_df.loc[difference_selector].iloc[0]}\n\n"
            )
        assert False

def _set_identical_columns_order(df1: pd.DataFrame, df2: pd.DataFrame):
    df2 = df2[df1.columns]
    return df1, df2

def _compare_types(expected_df: pd.DataFrame, actual_df: pd.DataFrame):
    actual_dtypes = actual_df.dtypes
    expected_dtypes = expected_df.dtypes
    for col_name in expected_df.columns:
        actual_type = actual_dtypes[col_name].name
        expected_type = expected_dtypes[col_name].name
        if actual_type != expected_type:
            raise ValueError(
                f"Found dtypes mismatch in col: {col_name}. expected: {expected_type}, actual: {actual_type}"
            )
        if actual_type == 'category':
            actual_categories = actual_df[col_name].cat.categories
            expected_categories = expected_df[col_name].cat.categories
            if not actual_categories.equals(expected_categories):
                raise ValueError(
                    f"Found categories mismatch in col: {col_name}. expected: {expected_categories}, actual: {actual_categories}"
                )
