import pandas as pd

def calc_lot_z_neighbors_optimized(df: pd.DataFrame) -> pd.DataFrame:
    df_with_z_neighbors = df.copy()
    grouping = df_with_z_neighbors.groupby([
        "LOT",
        "X_COORD",
        "Y_COORD"
    ])["TEST_PASSED"]

    df_with_z_neighbors["Z_SUM_YIELD"] = grouping.transform("sum")
    df_with_z_neighbors["Z_NEIGHBORS"] = grouping.transform("count")
    df_with_z_neighbors["Z_MEAN_YIELD"] = grouping.transform("mean")
    
    return df_with_z_neighbors

def calc_lot_z_neighbors(df: pd.DataFrame) -> pd.DataFrame:
    """
    Calculate average yield of the same (X,Y) coordinates per lot in order to identify low yield spots within a lot.

    :param df: Input dataset.
    :type df: pandas.DataFrame

    :return: data frame with the z neighbors series
    :rtype: pandas.DataFrame
    """

    agg_data = []
    for lot in df["LOT"].unique():
        lot_df = df[df["LOT"] == lot]
        for x in lot_df["X_COORD"].unique():
            x_df = lot_df[lot_df["X_COORD"] == x]
            for y in x_df["Y_COORD"].unique():
                y_df = x_df[x_df["Y_COORD"] == y]
                z_sum_yield = y_df["TEST_PASSED"].sum()
                z_neighbors = y_df["TEST_PASSED"].count()
                z_mean_yield = y_df["TEST_PASSED"].mean()
                agg_data.append({"LOT": lot, "X_COORD": x, "Y_COORD": y, "Z_SUM_YIELD": z_sum_yield, "Z_NEIGHBORS": z_neighbors, "Z_MEAN_YIELD": z_mean_yield})
    
    agg_df = pd.DataFrame(agg_data)
    
    df_with_z_neighbors = df.copy()
    df_with_z_neighbors = pd.merge(df_with_z_neighbors, agg_df, on=["LOT", "X_COORD", "Y_COORD"], how="left")

    return df_with_z_neighbors