import pandas as pd
import numpy as np


def calc_lot_gnbn(x, y, wafer, target_values):
    """
    Calculate the average yield of neighbor "dies" for the first ring (X, Y coordinates) per lot in order to identify high-risk units without 'good' neighbors.

    :param x: Series of values representing the 'X' scale in a lot.
    :type x: array-like of shape (n_samples, )

    :param y: Series of values representing the 'Y' scale in a lot.
    :type y: array-like of shape (n_samples, )

    :param wafer: Series of values representing the 'WAFER' in a lot.
    :type wafer: array-like of shape (n_samples, )

    :param target_values: Target values (usually "GOOD" or "PASS")
    :type target_values: array-like of shape (n_targets, )

    :return:
        - NEIGHBORS_MEAN_YIELD - Yield calculation for neighbors per unit
        - N_NEIGHBORS - Number of neighbors per unit
        - N_GOOD_NEIGHBORS - Number of 'good' neighbors (target is 1) per unit
    :rtype:
        - NEIGHBORS_MEAN_YIELD - array, shape (n_samples,)
        - N_NEIGHBORS - array, shape (n_samples,)
        - N_GOOD_NEIGHBORS - array, shape (n_samples,)
    """
    SUM_KEY = 'SUM'
    COUNT_KEY = 'COUNT'
    MEAN_KEY = 'MEAN'

    pairs_dict = {}
    res_dict = {}
    index_pair = []
    neighboor_mean = []
    neighboor_count = []
    neighboor_sum = []

    for i in range(len(target_values)):
        pair = (x[i], y[i], wafer[i])
        pairs_dict[pair] = target_values[i]
        index_pair.append(pair)

    for i in range(len(target_values)):
        pair = (x[i], y[i], wafer[i])

        if pair not in res_dict:
            res_dict[pair] = {
                SUM_KEY: 0,
                COUNT_KEY: 0
            }

        # calculate neighbors
        for row in range(-1, 2, 1):
            for col in range(-1, 2, 1):
                if row == 0 and col == 0:
                    continue
                tmp_pair = (pair[0] + row, pair[1] + col, wafer[i])
                if tmp_pair not in pairs_dict:
                    continue

                res_dict[pair][SUM_KEY] += pairs_dict[tmp_pair]
                res_dict[pair][COUNT_KEY] += 1

    for pair, vals in res_dict.items():
        if res_dict[pair][COUNT_KEY] > 0:
            ratio = res_dict[pair][SUM_KEY] / res_dict[pair][COUNT_KEY]
            res_dict[pair][MEAN_KEY] = ratio
        else:
            res_dict[pair][MEAN_KEY] = np.nan

    for i in range(len(index_pair)):
        pair = index_pair[i]
        neighboor_mean.append(res_dict[pair][MEAN_KEY])
        neighboor_count.append(res_dict[pair][COUNT_KEY])
        neighboor_sum.append(res_dict[pair][SUM_KEY])

    return neighboor_mean, neighboor_count, neighboor_sum



def calc_lot_gnbn__optimized(x, y, wafer, target_values):
    """
    Calculate the average yield of neighbor "dies" for the first ring (X, Y coordinates) per lot in order to identify high-risk units without 'good' neighbors.

    :param x: Series of values representing the 'X' scale in a lot.
    :type x: array-like of shape (n_samples, )

    :param y: Series of values representing the 'Y' scale in a lot.
    :type y: array-like of shape (n_samples, )

    :param wafer: Series of values representing the 'WAFER' in a lot.
    :type wafer: array-like of shape (n_samples, )

    :param target_values: Target values (usually "GOOD" or "PASS")
    :type target_values: array-like of shape (n_targets, )

    :return:
        - NEIGHBORS_MEAN_YIELD - Yield calculation for neighbors per unit
        - N_NEIGHBORS - Number of neighbors per unit
        - N_GOOD_NEIGHBORS - Number of 'good' neighbors (target is 1) per unit
    :rtype:
        - NEIGHBORS_MEAN_YIELD - array, shape (n_samples,)
        - N_NEIGHBORS - array, shape (n_samples,)
        - N_GOOD_NEIGHBORS - array, shape (n_samples,)
    """

    pairs_dict = {}

    input_df = pd.DataFrame({
            "x": x,
            "y": y,
            "wafer": wafer,
            "target_values": target_values
        },
    ).set_index(["wafer", "x", "y"])

    pairs_dict = input_df.groupby([
        input_df.index.get_level_values("wafer"),
        input_df.index.get_level_values("x"),
        input_df.index.get_level_values("y"),
    ])["target_values"].agg(
        num_of_coordinate_repetition='count',
        target_values='last',
    )

    neighbor_col_names = []
    shifted_df = pd.DataFrame({
        "original_x": pairs_dict.index.get_level_values("x"),
        "original_y": pairs_dict.index.get_level_values("y"),
        "original_wafer": pairs_dict.index.get_level_values("wafer"),
    }).set_index(["original_wafer", "original_x", "original_y"])

    for x_shift in [-1, 0, 1]:
        for y_shift in [-1, 0, 1]:
            if x_shift == 0 and y_shift == 0:
                continue
            neighbor_col_name = f"neighbor_{x_shift}_{y_shift}"
            neighbor_col_names.append(neighbor_col_name)
            shifted_df["neighbor_x"] = shifted_df.index.get_level_values("original_x") + x_shift
            shifted_df["neighbor_y"] = shifted_df.index.get_level_values("original_y") + y_shift
            shifted_df["neighbor_wafer"] = shifted_df.index.get_level_values("original_wafer")

            neighbors_as_index = shifted_df.set_index(["neighbor_wafer", "neighbor_x", "neighbor_y"])
            neighbors_exist_selector = neighbors_as_index.index.isin(pairs_dict.index)
            neighbors_exist_df = neighbors_as_index.loc[neighbors_exist_selector]
            neighbors_location_index = neighbors_exist_df.index
            original_location_index = shifted_df.loc[neighbors_exist_selector].index
            
            pairs_dict.loc[original_location_index, neighbor_col_name] = pairs_dict.loc[neighbors_location_index, "target_values"].values
            
    pairs_dict["all_neighbors_sum"] = pairs_dict[neighbor_col_names].dropna(how='all').sum(axis='columns')
    pairs_dict["all_neighbors_count"] = pairs_dict[neighbor_col_names].notnull().sum(axis='columns')

    cols_to_copy = ["all_neighbors_sum", "all_neighbors_count", "num_of_coordinate_repetition"]
    input_df[cols_to_copy] = pairs_dict[cols_to_copy]

    neighbor_mean = input_df["all_neighbors_sum"] / input_df["all_neighbors_count"]
    neighbor_count = input_df["all_neighbors_count"] * input_df['num_of_coordinate_repetition']
    neighbor_sum = input_df["all_neighbors_sum"] * input_df['num_of_coordinate_repetition']
    return neighbor_mean.values, neighbor_count.values, neighbor_sum.values
