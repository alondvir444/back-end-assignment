from collections import defaultdict
import time
import re

import numpy as np
import pandas as pd


def split_channels(df,channels=8,pattern="_CH_",sn='SN',verbose=True):
    """
    """
    start_time = time.time()
    cols_to_split = df.filter(like=pattern,axis=1).columns.tolist()
    cols_found = [i for i in cols_to_split if i in df.columns.tolist() ]
    if verbose:
        print("Found "+str(len(cols_found))+"/"+str(len(cols_to_split))+" columns to split in dataset")
        
    items = []
    count = 0
    rows = df.shape[0]
    
    for index, row in df[[sn]+cols_found].iterrows():
        count+=1
        for i in range(1,1+channels):
            item = {}
            item[sn] = row[sn]
            item['CHANNEL']=i
            reg = pattern+str(i)
            for col in cols_found:
                col_name = col.replace(reg,'')
                if col.endswith(reg):
                    item[col_name] = row[col]
                    
            items.append(item)
        if verbose and count%1000==0:
            print("Iterated "+str(count)+"/"+str(rows) + " records")
    df_channels = pd.DataFrame(items)
    if verbose:
        print("Splitted dataset: "+str(df_channels.shape))
        print("--- %s seconds ---" % (time.time() - start_time))
    return df_channels, cols_found


def split_channels__optimized__iterate_by_channels(df,channels=8,pattern="_CH_",sn='SN',verbose=True):
    """
    """

    cols_found = [col for col in df.columns if pattern in col]

    channel_to_input_cols: dict[int, set[str]] = defaultdict(set)
    original_col_to_output_cols: dict[int, str] = {}

    df = df[[sn] + cols_found]
    for col in cols_found:
        output_col, channel = re.search(fr'(CHANNEL_PARAM_\d+){pattern}(\d+)', col).groups()
        channel = int(channel)
        channel_to_input_cols[channel].add(col)
        original_col_to_output_cols[col] = output_col
    
    output_dfs_list = []
    for channel, cols in channel_to_input_cols.items():
        sn_channel_and_params = df[[sn]].copy()
        sn_channel_and_params["CHANNEL"] = channel
        output_params = df[list(cols)].rename(columns=original_col_to_output_cols)
        sn_channel_and_params[output_params.columns] = output_params
        output_dfs_list.append(sn_channel_and_params)
    
    output_df = pd.concat(output_dfs_list).replace("nan", None).replace(np.nan, None).reset_index(drop=False)

    output_df = output_df.sort_values(["index", sn, "CHANNEL"]).drop(columns=["index"]).reset_index(drop=True)

    return output_df, cols_found

