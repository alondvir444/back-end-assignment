from dataclasses import dataclass
import json
from fastapi import FastAPI, Query

from src.columns_filter import ColumnsFilter
from src import sqlite_dal
from src.data_processors.z_neighbors_calculator import calc_lot_z_neighbors_optimized
from src.data_processors.lot_delta_zones_calculator import calc_lot_delta_zones__optimized__vectorized
from src.data_processors.lot_gnbn_calculator import calc_lot_gnbn__optimized
from src.data_processors.lot_radius_calculator import calc_lot_radius_optimized
from src.data_processors.split_channels_calculator import split_channels__optimized__iterate_by_channels


app = FastAPI()

@app.get("/z_neighbors")
def z_neighbors(sn: str = Query(default=None), uid: int = Query(default=None)):
    df = sqlite_dal.get_manufacturing_data_1(filter=ColumnsFilter(sn=sn, uid=uid))

    res = calc_lot_z_neighbors_optimized(df)
    res_col_to_values_list = res.to_dict(orient="list")

    return ContentAndStatus(res_col_to_values_list)


@app.get("/lot_radius")
def lot_radius(sn: str = Query(default=None), uid: int = Query(default=None)):
    df = sqlite_dal.get_manufacturing_data_1(filter=ColumnsFilter(sn=sn, uid=uid), columns_to_select=["X_COORD", "Y_COORD"])

    res = calc_lot_radius_optimized(df).tolist()

    return ContentAndStatus(res)


@app.get("/delta_zones")
def delta_zones(sn: str = Query(default=None), uid: int = Query(default=None)):
    df = sqlite_dal.get_manufacturing_data_1(filter=ColumnsFilter(sn=sn, uid=uid), columns_to_select=["X_COORD", "Y_COORD", "WAFER", "TEST_PASSED"])

    zone_delta_to_wafers, zone = calc_lot_delta_zones__optimized__vectorized(
        x=df["X_COORD"].values,
        y=df["Y_COORD"].values,
        wafer=df["WAFER"].values,
        target_values=df["TEST_PASSED"].values
    )

    res = {
        "zone": zone.tolist(),
        "zone_delta_to_wafers": zone_delta_to_wafers.tolist()
    }

    return ContentAndStatus(res)


@app.get("/good_and_bad_neighbors")
def good_and_bad_neighbors(sn: str = Query(default=None), uid: int = Query(default=None)):
    df = sqlite_dal.get_manufacturing_data_1(filter=ColumnsFilter(sn=sn, uid=uid), columns_to_select=["X_COORD", "Y_COORD", "WAFER", "TEST_PASSED"])
    neighbors_mean_yield, n_neighbors, n_good_neighbors = calc_lot_gnbn__optimized(
        x=df["X_COORD"].values,
        y=df["Y_COORD"].values,
        wafer=df["WAFER"].values,
        target_values=df["TEST_PASSED"].values
    )
    res = {
        "neighbors_mean_yield": neighbors_mean_yield.tolist(),
        "n_neighbors": n_neighbors.tolist(),
        "n_good_neighbors": [int(n) for n in n_good_neighbors]
    }

    return ContentAndStatus(res)


@app.get("/split_channels")
def split_channels(sn: str = Query(default=None), uid: int = Query(default=None)):
    df = sqlite_dal.get_manufacturing_data_2(filter=ColumnsFilter(sn=sn, uid=uid))
    res_df, cols_found = split_channels__optimized__iterate_by_channels(df)
    res_col_to_values_list = res_df.to_dict(orient="list")

    res = {
        "cols_found": cols_found,
        "values": res_col_to_values_list
    }

    return ContentAndStatus(res)

@dataclass
class ContentAndStatus:
    content: object
    is_success: bool = True
    message: str = ""

    def __str__(self):
        json_content = json.dumps(self.content)
        return {
            "is_success": self.is_success,
            "content": json_content,
            "message": self.message
        }
    
    def __dict__(self):
        return {
            "is_success": self.is_success,
            "content": self.content,
            "message": self.message
        }

if __name__ == "__main__":
  import uvicorn
  uvicorn.run("main:app", host="0.0.0.0", port=8000)
