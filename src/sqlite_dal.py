import pandas as pd 
import sqlite3

from src.columns_filter import ColumnsFilter

DB_CONNECTION_STRING = './sqlite_db2'

MANUFACTURING_DATA_1 = "manufacturing_data_1"
MANUFACTURING_DATA_2 = "manufacturing_data_2"

class SQLite:

    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def __enter__(self):
        self.connection: sqlite3.Connection = sqlite3.connect(**(self.kwargs))
        return self.connection

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.close()


def get_manufacturing_data_1(
    filter: ColumnsFilter = ColumnsFilter(),
    columns_to_select: list[str] = ["*"]
) -> pd.DataFrame:
    return _get_data_from_table(
        table_name=MANUFACTURING_DATA_1,
        filter=filter,
        columns_to_select=columns_to_select
    )


def get_manufacturing_data_2(
    filter: ColumnsFilter = ColumnsFilter(),
    columns_to_select: list[str] = ["*"]
) -> pd.DataFrame:
    return _get_data_from_table(
        table_name=MANUFACTURING_DATA_2,
        filter=filter,
        columns_to_select=columns_to_select
    )


def _get_data_from_table(
    table_name: str,
    filter: ColumnsFilter,
    columns_to_select: list[str],
) -> pd.DataFrame:
    with SQLite(database=DB_CONNECTION_STRING) as sqlite_connection:
        query = f"""
            SELECT {", ".join(columns_to_select)}
            FROM {table_name}
            WHERE TRUE
            {_create_filter_string(filter)}
        """
        print("Running query:")
        print(query)
        query_results = sqlite_connection.cursor().execute(query)
        all_results = query_results.fetchall()
        col_names = [description[0] for description in query_results.description]

        df = pd.DataFrame(all_results, columns=col_names)
        print(f"Got {len(df)} rows")
        return df


def _create_filter_string(filter: ColumnsFilter) -> str:
    filter_string = ""
    if filter.sn:
        filter_string += f" AND SN = '{filter.sn}'"
    if filter.uid:
        filter_string += f" AND UID = {filter.uid}"
    return filter_string
