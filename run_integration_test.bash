#!/bin/bash

container_name="test_int_container"
image_name="test_int_image"

echo "Removing container if it exists"
docker rm -fv $container_name

echo "Building image"
docker build -t $image_name .

echo "Starting container"
docker run -d --name $container_name --memory 12288m --mount type=bind,src=C:/Users/alond/Alon/data_processing/back-end-assignment/db,target=/app/db -p 32768:8000 $image_name

if [ "$(docker inspect -f '{{.State.Running}}' $container_name)" == "true" ]; then
    echo "Container is running"
    
    echo "Waiting for the server to start"
    sleep 3s

    echo "Running tests"
    pytest -k test_integration__sanity

    echo "Tests finished"
else
    echo "Failed to start container"
fi

echo "Done"
