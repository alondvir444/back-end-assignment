FROM python:3.9.6 AS base

WORKDIR /app

COPY ./pyproject.toml .
COPY ./pdm.lock .

RUN pip install pdm
RUN pdm install 

COPY ./db/sqlite2.tar.gz ./db/
RUN tar xvzf ./db/sqlite2.tar.gz

EXPOSE 8000
CMD ["pdm", "run", "uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]

COPY ./src ./src

COPY ./main.py .
